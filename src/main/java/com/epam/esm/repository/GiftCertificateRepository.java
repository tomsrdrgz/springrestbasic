package com.epam.esm.repository;

import com.epam.esm.entity.GiftCertificate;
import org.springframework.data.repository.CrudRepository;

public interface GiftCertificateRepository extends CrudRepository<GiftCertificate, Long> {

}
