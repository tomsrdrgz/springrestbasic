package com.epam.esm.services;

import com.epam.esm.repository.GiftCertificateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GiftCertificateService {
    private final GiftCertificateRepository repository;

    @Autowired
    public GiftCertificateService(GiftCertificateRepository repository) {
        this.repository = repository;
    }

    // Implement business logic and methods
}
